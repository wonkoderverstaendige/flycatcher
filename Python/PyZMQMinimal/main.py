#
# Video Capture client
# Connects REQ socket to tcp://localhost:5555
#
import zmq
import numpy as np
import time
import cv2

REQUEST_TIMEOUT = 2500

context = zmq.Context()

# Socket to talk to server
print zmq.zmq_version()
print "Connecting to frame server..."
socket = context.socket(zmq.REQ)
socket.connect("tcp://localhost:5555")
socket.RCVTIMEO = 1000

#writer = cv2.VideoWriter('test.avi', cv2.cv.CV_FOURCC('X', 'V', 'I', 'D'), 60, (1280, 960), True)

# poller = zmq.Poller()
# poller.register(socket, zmq.POLLIN)

fps = 30
n_img = 0
img = None
key = -1
t_last = time.clock()
while key == -1:
    try:
        # Send request for new frame (any message will do here)
        socket.send(str(n_img), zmq.NOBLOCK)

        # Get the reply. Wait for it.
        # socks = dict(poller.poll(1000))
        # if socks and socks.get(socket) == zmq.POLLIN:
        #     img = np.fromstring(socket.recv(), np.uint8)
        #     n_img += 1
        #     print n_img
        #     img = np.reshape(img, (960, 1280, 3))
        img = np.fromstring(socket.recv(), np.uint8)
        img = np.reshape(img, (960, 1280, 3))
        n_img += 1
    except zmq.ZMQError:
        img = None
    except ValueError:
        img = None

    if img is not None:
        elapsed = time.clock() - t_last
        t_last = time.clock()
        fps = 0.9*fps + (0.1/elapsed)
        print fps
        cv2.imshow('preview', img)
        key = cv2.waitKey(1)

