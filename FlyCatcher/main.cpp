// ZeroMQ
#include <zmq.hpp>

// OpenCV
#include <opencv2\opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>

// FlyCapture SDK
#include "FlyCapture2.h"

// Standard library
#include <csignal>
#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <ctime>

#ifndef _WIN32
  #include <unistd.h>
#else
  #include <windows.h>
#endif

using namespace std;
using namespace FlyCapture2;

unsigned int imageCnt = 0;

bool bRecording = true;


void PrintError( Error error )
{
    error.PrintErrorTrace();
}

void PrintBuildInfo()
{
    FC2Version fc2Version;
    Utilities::GetLibraryVersion( &fc2Version );
    char version[128];
    sprintf( 
        version, 
        "FlyCapture2 library version: %d.%d.%d.%d\n", 
        fc2Version.major, fc2Version.minor, fc2Version.type, fc2Version.build );

    printf( version );

    char timeStamp[512];
    sprintf( timeStamp, "Application build date: %s %s\n\n", __DATE__, __TIME__ );

    printf( timeStamp );
}

void PrintCameraInfo( CameraInfo* pCamInfo)
{
    printf(
        "\n*** CAMERA INFORMATION ***\n"
        "Serial number - %u\n"
        "Camera model - %s\n"
        "Camera vendor - %s\n"
        "Sensor - %s\n"
        "Resolution - %s\n"
        "Firmware version - %s\n"
        "Firmware build time - %s\n\n",
        pCamInfo->serialNumber,
        pCamInfo->modelName,
        pCamInfo->vendorName,
        pCamInfo->sensorInfo,
        pCamInfo->sensorResolution,
        pCamInfo->firmwareVersion,
        pCamInfo->firmwareBuildTime );
}

void PrintStrobeInfo( StrobeInfo* pStrobeInfo ) {
    printf(
        "\n*** Current Strobe Settings ***\n"
        "Source - %u\n"
        "Presence - %d\n"
        "Readout support - %d\n"
        "onOff support - %d\n"
        "Polarity support - %d\n"
        "Min val - %e\n"
        "Max val - %e\n",
        pStrobeInfo->source,
        pStrobeInfo->present,
        pStrobeInfo->readOutSupported,
        pStrobeInfo->onOffSupported,
        pStrobeInfo->polaritySupported, 
        pStrobeInfo->minValue,
        pStrobeInfo->maxValue);
}

void PrintStrobeControl( StrobeControl* pStrobeControl ) {
    printf(
        "\n*** Current Strobe Settings ***\n"
        "Source - %u\n"
        "onOff - %d\n"
        "Polarity - %u\n"
        "Delay - %e\n"
        "Duration - %e\n",
        pStrobeControl->source,
        pStrobeControl->onOff,
        pStrobeControl->polarity,
        pStrobeControl->delay,
        pStrobeControl->duration );
}


int closeCamera(Camera cam) {
    Error error;
    return 0;
}

void OnImageGrabbed(Image* pImage, const void* pCallbackData)
{
    printf( "Grabbed image %d\n", imageCnt++ );
    return;
}

int RunCameraLoop( PGRGuid guid, std::string destination, bool bOCVSave, bool bOCVShow, bool bZMQ, bool bResize )
{
    std::cout << destination << std::endl;
    const int k_numImages = 0;  // zero for infinite number of images 

    double dRatio;
    if (bResize)
    {
        dRatio = 0.625; //0.625 or 0.5 are nice
    } else {
        dRatio = 1.0;
    }

    Error error;
    Camera cam;

    // Connect to a camera
    error = cam.Connect(&guid);
    if (error != PGRERROR_OK)
    {
        PrintError( error );
        return -1;
    }

    // Get the camera information
    CameraInfo camInfo;
    error = cam.GetCameraInfo(&camInfo);
    if (error != PGRERROR_OK)
    {
        PrintError( error );
        return -1;
    }
    PrintCameraInfo(&camInfo);

    // Set the strobe control
    StrobeControl strobeControl;
    strobeControl.source = 1;
    //strobeControl.parameter = 0;
    strobeControl.onOff = true;
    strobeControl.polarity = 1;
    strobeControl.delay - 0.0f;
    strobeControl.duration = 4.0f;
    cam.SetStrobe( &strobeControl );

    //// Check the strobe control
    //StrobeInfo strobeInfo;
    //error = cam.GetStrobeInfo( &strobeInfo );
    //if (error != PGRERROR_OK)
    //{
    //    PrintError( error );
    //    return -1;
    //}
    //PrintStrobeInfo( &strobeInfo );
    //PrintStrobeInfo( &strobeInfo );

	// Create Video Output
    // "Full" resolution (1280x960) results:
    //CV_FOURCC('D', 'I', 'V', 'X') // 66 fps,    22.2 MB,  OK
    //CV_FOURCC('M', 'J', 'P', 'G') // 37 fps,    155 MB,   OK
    //CV_FOURCC('P', 'I', 'M', '1') // 37-40 fps, 37.2 MB,  OK
    //CV_FOURCC('X', 'V', 'I', 'D') // 63-66 fps, 18.4 MB,  OK
    //CV_FOURCC('M', 'P', '4', '2') // 62 fps,    19.0 MB,  OK
    //CV_FOURCC('F', 'M', 'P', '4') // 65 fps,    19.8 MB,  OK
    //CV_FOURCC('I', '4', '2', '0') // ~120 fps,  1800 MB,  OK, HD limited
    cv::VideoWriter outputVideo;

    // Resize frame
    cv::Size size = cv::Size(static_cast<int>(1280*dRatio), static_cast<int>(960*dRatio));
    cout << "Rescaled to " << size.width << "x" << size.height << endl;

    // C++11 stuff
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::stringstream buffer;
    buffer << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S.avi");

    outputVideo.open(buffer.str(), CV_FOURCC('F', 'M', 'P', '4'), 60.0, size, true);
	if (!outputVideo.isOpened())
	{
		cout << "Could not open the video writer for some reason..." << endl;
		return -1;
	}

    // Setting up ZeroMQ connection
    zmq::context_t zcontext (1);
    zmq::socket_t zsocket (zcontext, ZMQ_REP);
    zsocket.bind ("tcp://*:5555");
	
	// Start capturing images
    error = cam.StartCapture(); //OnImageGrabbed
    if (error != PGRERROR_OK)
    {
        PrintError( error );
        return -1;
    }
    Image rawImage;    

	clock_t last_time = clock();
	float avg_time = 25.0;
    int key = -1;

	//for ( int imageCnt=0; imageCnt < k_numImages; imageCnt++ )
    while (key == -1 && (k_numImages == 0 || imageCnt < k_numImages) && bRecording) //
	{   
        imageCnt++;
        // Retrieve an image
        error = cam.RetrieveBuffer( &rawImage );
        if (error != PGRERROR_OK)
        {
            PrintError( error );
            continue;
        }

        // Create a converted image
        Image convertedImage;
        // Convert the raw image
		error = rawImage.Convert( PIXEL_FORMAT_BGR, &convertedImage );
        if (error != PGRERROR_OK)
        {
            PrintError( error );
            return -1;
        }  

        ImageMetadata metadata = rawImage.GetMetadata();
        cout << metadata.embeddedTimeStamp;

		unsigned int rowBytes = (double) convertedImage.GetReceivedDataSize()/(double)convertedImage.GetRows();
		cv::Mat opencvImg = cv::Mat(convertedImage.GetRows(), convertedImage.GetCols(),
									    CV_8UC3, convertedImage.GetData(), rowBytes );

        // Convert Image to OpenCV Mat
        if (bOCVShow || bOCVSave) {
            if (bResize)
            {
                cv::resize(opencvImg, opencvImg, cv::Size(), dRatio, dRatio, cv::INTER_AREA);
            }

            // Show image with highui method
            if (bOCVSave) 
            {
		        outputVideo << opencvImg;
            }
            // Show image with highui method
            if (bOCVShow) 
            {
		        cv::imshow("OpenCV", opencvImg);
                key = cv::waitKey(1);
            }

        }

        if (bZMQ)
        {
            // zmq messaging
            zmq::message_t zrequest;
            if (zsocket.recv(&zrequest, ZMQ_DONTWAIT) == true)
            {
                std::cout << "\rCONN ";
                //std::cout << static_cast<char*>(zrequest.data()) << std::endl;
                //cout << convertedImage.GetDataSize() << endl;
                //cout << convertedImage.GetDataSize() << ":" << convertedImage.GetRows() << "x" << convertedImage.GetCols() << endl;

                // Send Original Frame
                //zmq::message_t imgreply (convertedImage.GetDataSize());
                //memcpy ((void *) imgreply.data (), convertedImage.GetData(), convertedImage.GetDataSize());
                //zsocket.send (imgreply);

                // Send OpenCV frame
                zmq::message_t imgreply (opencvImg.rows*opencvImg.cols*3);
                memcpy ((void *) imgreply.data (), opencvImg.data, opencvImg.rows*opencvImg.cols*3);
                zsocket.send (imgreply);

                //zmq::message_t zreply (2);
                //memcpy ((void *) zreply.data (), "OK", 2);
                //zsocket.send (zreply);
            } else {
                std::cout << "\r     ";
            }
        }

        // FPS display
        avg_time = avg_time*0.9 + (clock()-last_time)*0.1;
        std::cout << std::setprecision(3) << 1000/avg_time << " fps ";
		last_time = clock();

        //// Save the image. If a file format is not passed in, then the file
        //// extension is parsed to attempt to determine the file format.
        //error = convertedImage.Save( filename );
        //if (error != PGRERROR_OK)
        //{
        //    PrintError( error );
        //    return -1;
        //}  
    }            
    
    // Stop capturing images
    error = cam.StopCapture();
    if (error != PGRERROR_OK)
    {
        PrintError( error );
        return -1;
    }

    // Stop strobing
    strobeControl.source = 1;
    //strobeControl.parameter = 0;
    strobeControl.onOff = false;
    strobeControl.polarity = 1;
    strobeControl.delay - 0.0f;
    strobeControl.duration = 4.0f;
    cam.SetStrobe( &strobeControl );

    // Disconnect the camera
    error = cam.Disconnect();
    if (error != PGRERROR_OK)
    {
        PrintError( error );
        return -1;
    }
    return 0;
}


// SIGINT handler
void int_handler(int x)
{
cout << "In interrupt handler" <<endl; // after pressing CTRL+C
bRecording = false;
}


int main(int argc, char* argv[])
{
    signal(SIGINT, int_handler);

    // Video out file destination from command line parameters
    std::string destination = "";
    if (argc > 1) {
        std::cout << argv[1] << std::endl;
        destination = argv[1];
    }

    PrintBuildInfo();
    Error error;

    // Since this application saves images in the current folder
    // we must ensure that we have permission to write to this folder.
    // If we do not have permission, fail right away.
	FILE* tempFile = fopen("permissioncheck.txt", "w+");
	if (tempFile == NULL)
	{
		printf("Failed to create file in current folder.  Please check permissions.\n");
		return -1;
	}
	fclose(tempFile);
	remove("permissioncheck.txt");

    // FlyCapture BusManager, interfaces with camera IO
    BusManager busMgr;
    unsigned int numCameras;
    error = busMgr.GetNumOfCameras(&numCameras);
    if (error != PGRERROR_OK)
    {
        PrintError( error );
        return -1;
    }

    printf( "Number of cameras detected: %u\n", numCameras );
    //for (unsigned int i=0; i < numCameras; i++)
    //{
    //}

    // Assuming only a single camera is connected...
    PGRGuid guid;
    error = busMgr.GetCameraFromIndex(0, &guid);
    if (error != PGRERROR_OK)
    {
        PrintError( error );
        return -1;
    }
    RunCameraLoop( guid, destination, true, false, true, true );

    cout << endl << "DONE";
	getchar();
    return EXIT_SUCCESS;
}
